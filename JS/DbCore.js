/**
* Created by Kratenok on 13-Sep-14.
*/
function DbCore()
{
    var database;

    var CreateDatabase = function()
    {
        console.log("test call");
        console.log(database);

        database.transaction(function(tr)
        {
            try {
                tr.executeSql("create table Notes ( Id integer primary key autoincrement, Title varchar(1024), Content blob )");
                console.log("transaction completed");
            }
            catch(ex)
            {
                alert("exception occured");
                console.log("db exception" + ex);
            }
        });
    }

    var Ctor = function()
    {
        //Initialization
        database = openDatabase('single-note', "0.1", "single note database", 2*1024*1024);
    }

    Ctor();
    CreateDatabase();

    this.ExecuteScript = function(script)
    {
        debugger;
        database.transaction(function(tr)
        { 
            tr.executeSql(script);
        });
    }

    this.ExecuteScriptWithResult = function(script)
    {
        
        database.transaction(function(tr)
        {
            tr.executeSql(script, [], function(tr, results)
            {
                return results;
            });

        });
    }
}
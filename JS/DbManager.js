function DbManager()
{
	this.OnReady = function()
	{
		var promise = new Promise(function(resolve, reject)
		{
			$org.context.onReady(resolve);
		})

		return promise;
	}

	this.SaveNote = function(note)
	{
		var promise = new Promise(
			function(resolve, reject)
			{
				$org.context.beginTransaction(true, function(trn)
				{
					var obj = new $org.types.NoteDto();
					obj.Title = note.Title;
					$data('$org.types.NoteDto').save(obj)
					.then(function(newNote)
					{
						dbg("promise should be started from here");
						console.log(newNote.Id);
						resolve(newNote);
					});
				});
			});

		return promise;		
	}

	this.GetNotes = function()
	{
		return $org.context.Notes;
	}

	this.updateNote = function(note)
	{
		var p = new Promise(function(resolve, reject)
		{
			$org.context.beginTransaction(true, function(trn)
			{
				var noteToUpdate = $org.context.Notes.attachOrGet({Id : note.Id});
				noteToUpdate.Title = note.Title;
				$org.context.saveChanges().then(function()
				{
					resolve();
				});
			});
		});

		return p;
	}

	this.deleteNoteById = function(noteId)
	{
		return deleteNote({Id: noteId});
	}

	var deleteNote = this.deleteNote = function(note)
	{
		debugger;
		var p = new Promise(function(resolve, reject)
		{
			$org.context.beginTransaction(true, function(trn)
			{
				$org.context.Notes.remove(note);
				$org.context.saveChanges().then(function()
				{
					debugger;
					resolve();
				});
			});
		});
		return p;		
	}
}
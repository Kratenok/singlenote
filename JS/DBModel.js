$data.Entity.extend("$org.types.NoteDto",
{
	Id: {type: "int", key: true, computed:true },
	Title: {type: "string"}
});

$data.EntityContext.extend("$org.types.OrgContext", {
    Notes: { type: $data.EntitySet, elementType: $org.types.NoteDto }
});

$org.context = new $org.types.OrgContext
(
	{ 
		name: "webSql", 
		databaseName: "SingleNote", 
		dbCreation: $data.storageProviders.DbCreationType.DropTableIfChanged 
	}
);

//Mapping section
function Note()
{
	Note.prototype = $org.types.NoteDto;
}
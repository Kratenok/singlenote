create table Notes
(
    Id integer primary key autoincrement,
    Title varchar(255),
    Content blob
)